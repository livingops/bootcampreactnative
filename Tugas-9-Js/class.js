/* Soal No. 1*/
class Animal {
    constructor(sheep) {
    this.name = sheep;
	this.legs = 4;
	this.cold_blooded = "false"; 
  }
}

class Ape extends Animal {
   yell() {
   console.log("Auoo");
  }
}

class Frog extends Animal {
   jump() {
   console.log("hop hop");
  }
}

console.log("1. Animal Class")
var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
var sungokong = new Ape("kera sakti");
sungokong.yell();
var kodok = new Frog("buduk");
kodok.jump();


/* Soal No. 1*/
class Jam {
  constructor({template}) {
    this.template = template;
  }

  render() {
    var tgl = new Date();
    var jam = tgl.getHours(); 
	var min = tgl.getMinutes(); 
	var det = tgl.getSeconds();
    if (jam < 10) jam = '0' + jam;
	if (min < 10) min = '0' + min;
	if (det < 10) det = '0' + det;
    var output = this.template
	  .replace('h', jam)
      .replace('m', min)
      .replace('s', det);
    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}
var clock = new Jam({template: 'h:m:s'});
console.log("\n2. Function to Class")
clock.start();  