/*Soal No. 1 Looping While */
var angk1 = 1;
var angk2 = 20;
console.log("Soal No. 1 Looping While");
console.log("LOOPING PERTAMA");
while(angk1 <= 20)
{
	console.log(angk1+" - I love coding");
	angk1+=1;
}
console.log("LOOPING KEDUA");
while(angk2 > 0)
{
	console.log(angk2+" - I will become a mobile developer");
	angk2-=1;
}

/*Soal No. 2 Looping menggunakan for */
var gjl = "Santai";
var gnp =  "Berkualitas";
var modl = "I Love Coding";
var astop = 20;
console.log("\nNo. 2 Looping menggunakan for");
console.log("Output");
for (var stp = 1; stp <= astop; stp++)
{
	if( stp%2 == 0 )
	{
		console.log(stp + ". "+ gnp);
	}else if( stp%2 !== 0 && stp%3 == 0 )
	{
		console.log(stp + ". "+ modl);
	}else{
		console.log(stp + ". "+ gjl);
	}
}	

/*Soal No. 3 Membuat Persegi Panjang*/
var pjg = 4;
var lbr = 8;
var ktk = "";
console.log("\nSoal No. 3 Membuat Persegi Panjang")
for (var p = 0; p < pjg; p++) {
    for (var l = 0; l < lbr; l++) {
        ktk+="#"
    }
    console.log(ktk);
	ktk = "";
}

/*Soal No. 4 Membuat Tangga*/
var t = 7;
var a = t;
var sgt = "";
console.log("\nSoal No. 4 Membuat Tangga")
do {
  for (a = t; a <= 7; a++) {sgt+="#"};
  console.log(sgt);
  sgt = "";
  a--;
  t--;
}
while (t > 0); 

/*Soal No. 5 Membuat Papan Catur*/
var ukrn = 8;
var grs = "";
console.log("\nSoal No. 5 Membuat Papan Catur")
for (var i = 0; i < ukrn; i++) {
    for (var j = 0; j < ukrn; j++) {
		if((i+j)%2 == 0){
			 grs +="#";
		}else{
			grs +=" ";
		}
    }
    grs += "\n";
}
console.log(grs);