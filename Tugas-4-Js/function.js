/*Soal No. 1*/
function teriak(){return "Halo Sanbers!";}
console.log("Soal No. 1 :");
console.log(teriak());

/*Soal No. 2*/
var num1 = 12
var num2 = 4
function kalikan(a,b){return a*b;}
var hasilKali = kalikan(num1, num2)
console.log("\nSoal No. 2 :");
console.log(hasilKali);

/*Soal No. 3*/
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
function intro(i,x,y,z)
{return "Nama saya "+i+",\numur saya "+x+" tahun,\nalamat saya di "+y+" dan\nsaya punya hobby yaitu "+z+"!";}
var introduce = intro(name,age,address,hobby);
console.log("\nSoal No. 3 :");
console.log(introduce);