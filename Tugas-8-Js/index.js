var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function baca(time,books,x)
{
	if(x < books.length)
	{
		readBooks(time, books[x], function(timeleft)
		{
			if(timeleft > 0)
			{
				x +=1;
				baca(timeleft, books, x);
			}
		})
	}
}
console.log("Soal No. 1 (Callback Baca Buku)");
baca (10000,books,0);