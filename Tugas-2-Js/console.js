/*Variabel Conditional If-Else*/
var nama = "ARIF";
var peran = "GuarD";
var snama = String(nama);
var speran = String(peran);

/*Variabel Conditional Switch-case*/
var date = 8;
var month = 12;
var year = 2020;


/*Soal Conditional If-Else*/
console.log("SOAL IF-ELSE :");
if (!!snama.trim())
{	
	if(!!speran.trim())
	{
			if(speran.toLowerCase() == "werewolf")
			{
				console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
			} else if(speran.toLowerCase() == "guard") 
			{
				console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan Werewolf.");
			} else if(speran.toLowerCase() == "penyihir")
			{
				console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi Werewolf!");
			} else
			{
				console.log("Halo " + nama + ", isi peranmu hanya dengan variabel : Werewolf, Guard atau Penyihir.");
			}
	} else
	{
		console.log("Halo " + nama + ",");
		console.log("Variabel [Peran] harus diisi!");
	}
} else {
	console.log('Variabel [Nama] harus diisi!');
}


/*Soal Conditional Switch-case*/
console.log("\nSOAL SWITCH-CASE");
if(date >= 1 && date <= 31 && month >= 1 && month <=12 && year >= 1900 && year <= 2200 )
{
	switch(month) {
	  case 1:   { console.log(date + " " + "Januari " + year); break; }
	  case 2:   { console.log(date + " " + "Februari " + year); break; }
	  case 3:   { console.log(date + " " + "Maret " + year); break; }
	  case 4:   { console.log(date + " " + "April " + year); break; }
	  case 5:   { console.log(date + " " + "Mei " + year); break; }
	  case 6:   { console.log(date + " " + "Juni " + year); break; }
	  case 7:   { console.log(date + " " + "Juli " + year); break; }
	  case 8:   { console.log(date + " " + "Agustus " + year); break; }
	  case 9:   { console.log(date + " " + "September " + year); break; }
	  case 10:  { console.log(date + " " + "Oktober " + year); break; }
	  case 11:  { console.log(date + " " + "November " + year); break; }
	  case 12:  { console.log(date + " " + "Desember " + year); break; }
	default:  { console.log('Error'); }}
}	else {
	console.log("Variabel Tidak Tepat!\nRentang Tanggal adalah 1 - 31;\nRentang Bulan adalah 1 - 12;\nRentang Tahun adalah 1900 - 2200.");
}