/*Variabel Soal 1*/
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

/*Variabel Soal 2*/
var x = "I am going to be React Native Developer"; 
var firstWord = x[0]; 
var secondWord = x[2] + x[3]; 
var thirdWord = x[5] + x[6] + x[7] + x[8] + x[9]; 
var fourthWord = x[11] + x[12];
var fifthWord = x[14] + x[15];
var sixthWord = x[17] + x[18] + x[19] + x[20] + x[21];
var seventhWord = x[23] + x[24] + x[25] + x[26] + x[27] + x[28];
var eighthWord = x[30] + x[31] + x[32] + x[33] + x[34] + x[35] + x[36] + x[37] + x[38];

/*Variabel Soal 3*/
var sentence = 'wow JavaScript is so cool'; 
var firstWord2 = sentence.substring(0,3); 
var secondWord2 = sentence.substring(4,14); 
var thirdWord2 = sentence.substring(15,17);
var fourthWord2 = sentence.substring(18,20);
var fifthWord2 = sentence.substring(21,25);

/*Variabel Soal 4*/
var stc = 'wow JavaScript is so cool'; 
var firstWord3 = stc.substring(0,3); 
var secondWord3 = stc.substring(4,14); 
var thirdWord3 = stc.substring(15,17); 
var fourthWord3 = stc.substring(18,20);
var fifthWord3 = stc.substring(21,25);
var firstWordLength = firstWord3.length;  
var secondWordLength = secondWord3.length;  
var thirdWordLength = thirdWord3.length;  
var fourthWordLength = fourthWord3.length;  
var fifthWordLength = fifthWord3.length; 

console.log('Soal Nomor 1 :');
console.log(word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh + "\n");

console.log('Soal Nomor 2 :');
console.log(x);
console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord + "\n");

console.log('Soal Nomor 3 :');
console.log(sentence);
console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2 + "\n");

console.log('Soal Nomor 4 :');
console.log(stc); 
console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 