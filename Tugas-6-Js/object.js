/* Soal No. 1 */
function arrayToObject(arr) {
	var x = {};
	var now = new Date();
	var thisYear = now.getFullYear();
	
	if (arr.length <= 0){
		return " ";
	}else if (arr.length > 0)
	{
		for (var i = 0; i < arr.length; i++)
		{
			for (var j = 0; j < arr[i].length; j++)
			{ 
				switch(j){
					case 0: x["fname"] = arr[i][j]; break; 
					case 1: x["lname"] = arr[i][j]; break;
					case 2: x["gender"] = arr[i][j]; break; 
					case 3: x["age"] = arr[i][j]; break;						
				}
			}
			console.log(i+1 +". "+ x.fname + " " + x.lname + ": {");
			console.log("firsName: " + x.fname);
			console.log("lastName: " + x.lname);
			console.log("gender: " + x.gender);
			if (x["age"] >= thisYear || x["age"] == "")
				{
					console.log("Invalid birth year");
				}else{
					console.log("age: " + (thisYear-x["age"]));
					x["age"]="";
				}
			console.log("}\n");
		}
	}
}
 
var people = [["Bruce", "Banner", "male", 1975],["Natasha", "Romanoff", "female"]];
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
console.log("Soal No. 1 (Array to Object)");
arrayToObject(people); 
arrayToObject(people2);
arrayToObject([]); 


/* Soal No. 2 */
function shoppingTime(memberid, money)
{
	var itemsell = [["Sepatu Stacattu", 1500000],["Baju Zoro", 500000],["Baju H&N", 250000],["Sweater Uniklooh", 175000],["Casing Handphone", 50000]];
	var arr = itemsell.sort(function(a,b){return b[1] - a[1];});
	var budget = money;
	var itembuy = [];
	var listitem = {};
	var idxblj = 1;
	var a = 0; var b = 0; var c = 0; var d = 0; var e = 0;	

	if (memberid == "")
	{
		console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
	} else if (money == "")
	{
		console.log("Mohon maaf, uang tidak cukup");
	}else{
		while (idxblj != 0){
			//for (var i = 0; i < arr.length; i++)
			//{
				if(money >= arr[0][1] && a == 0)
				{
					itembuy.push("'"+arr[0][0]+"'"); money-=arr[0][1]; a++; 
				}else if(money >= arr[1][1]  && b == 0) 
				{
					itembuy.push("'"+arr[1][0]+"'"); money-=arr[1][1]; b++; 
				}else if(money >= arr[2][1]  && c == 0)
				{
					itembuy.push("'"+arr[2][0]+"'"); money-=arr[2][1]; 
				}else if(money >= arr[3][1]  && d == 0)
				{
					itembuy.push("'"+arr[3][0]+"'"); money-=arr[3][1]; 
				}else if(money >= arr[4][1]  && e == 0)
				{
					itembuy.push("'"+arr[4][0]+"'"); money-=arr[4][1]; 
				}else{
					idxblj = 0;
				}
			//}
		}
		if(itembuy.length == 0){ console.log("Mohon maaf, uang tidak cukup");} else {	
			listitem.id = memberid;
			listitem.mny = budget;
			listitem.lst = itembuy.join();
			listitem.sisa = money
			console.log("{ memberID: " + listitem.id + ",\nmoney: " + listitem.mny + ",\nlistPurchased:");
			console.log("[" + listitem.lst + "],")
			console.log("changeMoney: " + listitem.sisa + "\n}\n")
		}
	}
}
console.log("\nSoal No. 2 (Shopping Time)");
shoppingTime('1820RzKrnWn08',2475000);
shoppingTime('82Ku8Ma742',170000);
shoppingTime('', 2475000);
shoppingTime('234JdhweRxa53',15000);
shoppingTime();


/* Soal No. 3 */
function naikAngkot(arrPenumpang) {
  var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var x1 = {};
  var bayar = 0;
	if (arrPenumpang.length == "")
	{
		return "";
	}else if (arrPenumpang.length > 0)
	{
		for (var i = 0; i < arrPenumpang.length; i++)
		{
			for (var j = 0; j < arrPenumpang[i].length; j++)
			{ 
				switch(j)
				{
					case 0: x1["p"] = arrPenumpang[i][j]; break; 
					case 1: x1["fr"] = arrPenumpang[i][j]; break;
					case 2: x1["to"] = arrPenumpang[i][j]; break; 						
				}
			}
			bayar = 2000*(rute.indexOf(x1.to)-rute.indexOf(x1.fr));
			if(i == 0)
			{
				console.log("[{ penumpang: " + x1.p + ", naikDari: " + x1.fr + ", tujuan: " + x1.to + ", bayar: " + bayar + " },");
			} else if(i != 0 && i < (arrPenumpang.length-1))
			{
				console.log("{ penumpang: " + x1.p + ", naikDari: " + x1.fr + ", tujuan: " + x1.to + ", bayar: " + bayar + " },");
			} else 
			{
				console.log("{ penumpang: " + x1.p + ", naikDari: " + x1.fr + ", tujuan: " + x1.to + ", bayar: " + bayar + " }]");
			}
			
		}
	}
}
console.log("\nSoal No. 3 (Naik Angkot)");
naikAngkot([['Dimitri', 'B', 'F'],['Icha', 'A', 'B'],['Jodi', 'A', 'F']]);
naikAngkot([]);