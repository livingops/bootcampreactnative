/* Soal No. 1 */
appFunction = () => {console.log("this is golden!!");}; 
console.log("1. Mengubah fungsi menjadi fungsi arrow");
appFunction();


/* Soal No. 2 */
const newFunction = literal = (firstName,lastName) => {const fulln =`${firstName} ${lastName}`;console.log(fulln);return};
console.log("\n2. Sederhanakan menjadi Object literal di ES6");
newFunction("William","Imoh");


/* Soal No. 3 */
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};
const { firstName, lastName, destination, occupation, spell} = newObject;
console.log("\n3. Destructuring");
console.log(firstName, lastName, destination, occupation);


/* Soal No. 4 */
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let cArray = [...west,...east];
console.log("\n4. Array Spreading");
console.log(cArray);


/* Soal No. 5 */
const planet = "earth";
const view = "glass";
var before = 'Lorem ' + `${view}` + ' dolor sit amet, ' +  
    'consectetur adipiscing elit, ' + `${planet}` + ' do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam';
console.log("\n5. Template Literals");
console.log(before) 
