/*Soal No. 1*/
function range(startNum, finishNum){
    var number = [];
    if(startNum < finishNum)
	{
		for (var i = startNum; i <= finishNum; i++) {number.push(i);}
		return number;
	} else if(startNum > finishNum)
	{
		for (var i = startNum; i >= finishNum; i--) {number.push(i);}
		return number;
	}else{
		return -1;
	}
}
console.log("Soal No. 1 (Range)");
console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());


/*Soal No. 2*/
function rangeWithStep(startNum, finishNum, step){
    var number = [];
    if (step == null || step === 'undefined'){ 
		return -1;
	}else{
	    if(startNum < finishNum)
		{
			for (var i = startNum; i <= finishNum; i+=step) {number.push(i);}
			return number;
		} else if(startNum > finishNum)
		{
			for (var i = startNum; i >= finishNum; i-=step) {number.push(i);}
			return number;
		}
	}
}
console.log("\nSoal No. 2 (Range with Step)");
console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

/*Soal No. 3*/
function sum(startNum, finishNum, step){
    var number = [];
	var sumofrange = 0;
	if (startNum == null || startNum === 'undefined')
	{
		return  0;
	}else if (finishNum == null || finishNum === 'undefined')
	{
		return  startNum;
	}else if (step == null || step === 'undefined'){ 
		number = range(startNum, finishNum);
	}else{
		if (startNum < finishNum)
		{
			for (var i = startNum; i <= finishNum; i+=step) {number.push(i);}
		} else if (startNum > finishNum) 
		{
			for (var i = startNum; i >= finishNum; i-=step) {number.push(i);}
		}
	}
	for (var j = sumofrange; j < number.length; j++) {sumofrange+=number[j];}
		return sumofrange;
}
console.log("\nSoal No. 3 (Sum of Range)");
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());


/*Soal No. 4*/
function dataHandling (string,numarr) { return string[numarr]; }
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]
console.log("\nSoal No. 4 (Array Multidimensi) "+ input.length);
for (var x = 0; x < input.length; x++)
{
	var sentence = dataHandling(input,x);
	for (var y = 0; y < sentence.length; y++)
	{
		switch (y){
			case 0 : console.log("Nomor ID\t: " + sentence[y]);break;
			case 1 : console.log("Nama Lengkap\t: " + sentence[y]);break;
			case 2 : console.log("TTL\t\t: " + sentence[y] + ", " + sentence [y+=1]);break;
			case 4 : console.log("Hobi\t\t: " + sentence[y]);break;
		}
	}
}


/*Soal No. 5*/
function balikKata(str){
	var p = "";
	for (var x = str.length - 1; x >= 0; x--) { p+=str[x]; }
	return p;
}
console.log("\nSoal No. 5 (Balik Kata)");
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers")); 


/*Soal No. 6*/
var a_input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(strg){
	var s_input = strg;
	s_input.splice(1,1,"Roman Alamsyah Elsharawy");
	s_input.splice(2,1,"Provinsi Bandar Lampung");
	s_input.splice(4,2,"Pria","SMA Internasional Metro");
	console.log(s_input);

	var s_date = s_input[3].split("/");
	switch(parseInt(s_date[1]))
	{
		case 1 : console.log("Januari");break;
		case 2 : console.log("Februari");break;
		case 3 : console.log("Maret");break;
		case 4 : console.log("April");break;
		case 5 : console.log("Mei");break;
		case 6 : console.log("Juni");break;
		case 7 : console.log("Juli");break;
		case 8 : console.log("Agustus");break;
		case 9 : console.log("September");break;
		case 10 : console.log("Oktober");break;
		case 11 : console.log("November");break;
		case 12 : console.log("Desember");
	}
	var join_date = s_date.join("-");
	console.log(s_date.sort(function(a, b){return b-a}));
	console.log(join_date);
	var s_name = s_input[1].split(" ");
	var s2_name = s_name[1].slice(0,15);
	console.log(s_name[0]+" "+s2_name);
}

console.log("\nSoal No. 6 (Metode Array)");
console.log("KALIMAT :");
console.log(a_input);
dataHandling2(a_input);