import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import Icon from 'react-native-vector-icons/Ionicons';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from "react-native";
 
export default function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
 
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require("./images/logo.png")} />
 <Icon style={styles.photos} name="md-person-circle" size={200} />
      <StatusBar style="auto" />
	  <View>
        <Text style={styles.textname}>Ano Nymous</Text>
      </View>
      <View>
        <Text style={styles.textmember}>Sanber Bootcamp Member</Text>
      </View>
 
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
 
  image: {
    marginBottom: 50,
	width: 170,
	height: 50
  },
  
  photos: {
    marginBottom: 20,
	color: "#003366"
  },
  
  textname: {
	fontSize: 50,
	fontWeight: "bold",
	color: "#003366",
	marginBottom: 10,
  },

  textmember: {
	fontSize: 20,
	fontWeight: "bold",
	color: "#3EC6FF"
  },
});